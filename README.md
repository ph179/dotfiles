# Welcome to my Gitlab page

Here are my sections so far:

---
### GNU/Linux

I use Debian on a daily basis. I have used other GNU/Linux systems:

- Linux Mint
- Ubuntu
- Debian
- Fedora
- OpenSuse
- Arch
- Gentoo

But Debian is fit for me. I don't look for the latest and greatest when installing packages. I just want a rock stable operating system that doesn't break on any update. And Debian does that job really good! So... I've used Arch for a few years as my main system but Debian is better suited for me.

---
#### Debian

Click on the Debian Logo to see some screenshots of my system.

[![Debian](img/debian_logo.png)](https://gitlab.com/ph179/debian/-/tree/main?ref_type=heads)